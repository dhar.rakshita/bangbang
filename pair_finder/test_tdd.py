import random
from pair_finder.best_pair_finder import best_pair_finder, particle_sorter
from datetime import datetime

def test_do_nothing():
    pass

def test_4_particle_example():
    expected_best_pair= [[1,3],[2,4]]
    particles_with_positions=[[1,-10],[2,-1],[3,-2],[4,1]]
    paired_particles_list, pair_indices, sum_pair_distance=best_pair_finder(particles_with_positions)
    assert expected_best_pair==pair_indices and sum_pair_distance==10

def particle_list_generator(number_of_particles):
    particle_list = []
    for x in range(number_of_particles):
        particle_number = x + 1
        particle_position = random.randint(0, 1000)
        particle = [particle_number, particle_position]
        particle_list.append(particle)
    return particle_list

def test_sorting_particles_by_printing():
    particle_list = particle_list_generator(10)
    print("This is the unsorted list of 2 element lists, corresponding to particles and their positions")
    print(particle_list)
    sorted_list = particle_sorter(particle_list)
    print("The sorted list of particles, each with their number first and their position second")
    print(sorted_list)

def test_pairing_for_10_particles():
    particle_list = particle_list_generator(10)
    print("This is the unsorted list of particles")
    print(particle_list)
    list_pairs, pair_indices, sum_pair_differences = best_pair_finder(particle_list)
    print("Below is the list of best pairs, with the sum of distances between pairs being", sum_pair_differences)
    print(pair_indices)

def test_find_best_pairs_for_100_particles_within_30_seconds():
    start = datetime.now()
    particle_list = particle_list_generator(100)
    list_pairs, pair_indices, sum_pair_differences = best_pair_finder(particle_list)
    end = datetime.now()
    time_taken = end - start
    time_taken_seconds = time_taken.total_seconds()
    print("Time taken for sorting into 100 pairs is,", time_taken_seconds, "seconds")
    assert time_taken_seconds<30
