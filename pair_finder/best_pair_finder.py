from operator import itemgetter


def particle_sorter(particles_unsorted):
    particles_sorted = sorted(particles_unsorted, key=itemgetter(1))
    return particles_sorted


def best_pair_finder(particles_unsorted):
    particles = particle_sorter(particles_unsorted)
    number_of_particles = len(particles)
    best_pairs_particles_list = []
    best_pair_indices = []
    sum_pair_distance = 0
    for x in range(0, number_of_particles, 2):
        first_particle = particles[x]
        second_particle = particles[x + 1]
        distance_in_pair = second_particle[1] - first_particle[1]
        sum_pair_distance += distance_in_pair

        individual_pair = [first_particle, second_particle]
        best_pairs_particles_list.append(individual_pair)

        pair_index = [first_particle[0], second_particle[0]]
        best_pair_indices.append(pair_index)
    return best_pairs_particles_list, best_pair_indices, sum_pair_distance
