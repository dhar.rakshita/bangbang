#Developer Documentation
This is written with reference to Arc42 at https://arc42.org/overview

##Introduction and Goals
- The purpose of this code is to demonstrate the Munchkins course has been learnt and applied.
- This is the Munchkins course: https://the-munchkins.gitlab.io/do-research-like-a-munchkin/
- One sub-goal with regards to this is to sort 100 particles with positions into pairs, which minimises the sum of distance between any particles in a pair
- This is explained in further detail here: https://the-munchkins.gitlab.io/do-research-like-a-munchkin/online_class/workshop_project/

##Constraints
- The main constraint here is time available to work on this project 
- The one is to achieve the sorting of 100 particles as noted above within 30 seconds on one computer

##Context and Scope
This is within the 'bangbang' group, who are learning the Munchkins course

##Solution Strategy
It was decided to sort particles by their positions. 
This is because it trivialises the solution in the 1D case. 
Whereupon you merely need to pair every two particles as you go in order along the sorted list.

##Deployment View
This is backed up on Gitlab, and updated using the FeatureFlow style