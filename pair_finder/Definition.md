# Best pair finder
The purpose of this code is to order a list of particles into sets of pairs, while minimising the distance between any two pairs.

## best_pair_finder(particle_list)
### Input
- The input will be a list of 2 element lists corresponding to particles
- For each 2 element list, the first element is the (initial) particle number and the second element the position of the particle.
### Output that is returned
####List of paired particles
- Each element of this list is a 2 element list referring to a pair of particles
- The elements of the pairs refer to the particles themselves, which are 2 element lists 
- For each particle, the first element is it's particle number as defined in the input, and the 2nd element is the particle position
####List of indices of best pairs
- Effectively the same as the above, except each particle only has particle number
- Physically a list of 2 element lists referring to the pairings
- And each pair list contains the particle numbers of the pairs
####Sum of distances between pairs
###Assumption
- Particle positions are 1D real numbers
